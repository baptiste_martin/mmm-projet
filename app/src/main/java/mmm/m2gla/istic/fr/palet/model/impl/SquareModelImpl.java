package mmm.m2gla.istic.fr.palet.model.impl;

import java.util.ArrayList;

import mmm.m2gla.istic.fr.palet.SampleApplication.objects.Board;
import mmm.m2gla.istic.fr.palet.model.BoardModel;
import mmm.m2gla.istic.fr.palet.model.PaletModel;
import mmm.m2gla.istic.fr.palet.model.SquareModel;

/**
 * Created by david on 18/03/15.
 */
/*
 * Cette classe représente un case du plateau, mais aussi une case du palets qui, selon le choix
 * de granularité, peut en occuper plusieurs
 * Une meme case peut admettre plusieurs palets par ordre d'arrivee
 */
public class SquareModelImpl implements SquareModel {
    private float                   x;                  // Coordonnee X dans ou hors le plateau
    private float                   y;                  // Coordonnee Y dans ou hors le plateau
    private float                   z;                  // Non utilise actuellement
    private int                     nbPalet;            // Nombre de palet
    private BoardModel              boardRef;           // Plateau d'appartenance
    private ArrayList<PaletModel>   paletsList;


    public SquareModelImpl(float x, float y, BoardModel myBoard) {
        paletsList = new ArrayList<>();
        this.x = x;
        this.y = y;
        this.z = 0;
        this.nbPalet = 0;
        this.boardRef = myBoard;
    }


    @Override
    public void addPalet(PaletModel newPalet) {
        paletsList.add(newPalet);
        this.nbPalet++;
    }


    @Override
    public float getX() {
        return this.x;
    }

    @Override
    public void setX(float x) {
        this.x = x;
    }

    @Override
    public float getY() {
        return this.y;
    }

    @Override
    public void setY(float y) {
        this.y = y;
    }

    @Override
    public float getZ() {
        return this.z;
    }

    @Override
    public void setZ(float z) {
        this.z = z;
    }

    @Override
    public ArrayList<PaletModel> getPaletsList() {
        return (ArrayList<PaletModel>) paletsList.clone();
    }

    @Override
    public int getNbPalet() {
        return nbPalet;
    }

    @Override
    public BoardModel getBoardRef() {
        return boardRef;
    }
}
