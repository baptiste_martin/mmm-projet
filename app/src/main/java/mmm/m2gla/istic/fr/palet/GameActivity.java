package mmm.m2gla.istic.fr.palet;

import android.app.Activity;
import android.content.Context;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.graphics.Color;
import android.hardware.Sensor;
import android.hardware.SensorEventListener;
import android.hardware.SensorEvent;
import android.hardware.SensorManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.GestureDetector;
import android.view.GestureDetector.SimpleOnGestureListener;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.qualcomm.vuforia.CameraDevice;
import com.qualcomm.vuforia.DataSet;
import com.qualcomm.vuforia.ImageTracker;
import com.qualcomm.vuforia.STORAGE_TYPE;
import com.qualcomm.vuforia.State;
import com.qualcomm.vuforia.Trackable;
import com.qualcomm.vuforia.Tracker;
import com.qualcomm.vuforia.TrackerManager;
import com.qualcomm.vuforia.Vuforia;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Vector;

import mmm.m2gla.istic.fr.palet.SampleApplication.SampleApplicationControl;
import mmm.m2gla.istic.fr.palet.SampleApplication.SampleApplicationException;
import mmm.m2gla.istic.fr.palet.SampleApplication.SampleApplicationSession;
import mmm.m2gla.istic.fr.palet.SampleApplication.utils.LoadingDialogHandler;
import mmm.m2gla.istic.fr.palet.SampleApplication.utils.SampleApplicationGLView;
import mmm.m2gla.istic.fr.palet.SampleApplication.utils.Texture;
import mmm.m2gla.istic.fr.palet.model.BoardModel;
import mmm.m2gla.istic.fr.palet.model.Coordinate;
import mmm.m2gla.istic.fr.palet.model.PartieModel;
import mmm.m2gla.istic.fr.palet.model.impl.BoardModelImpl;
import mmm.m2gla.istic.fr.palet.model.impl.CoordinateImpl;
import mmm.m2gla.istic.fr.palet.model.impl.PaletModelImpl;
import mmm.m2gla.istic.fr.palet.model.impl.PartieModelImpl;
import mmm.m2gla.istic.fr.palet.model.utils.Curve;

/**
 * Created by arno on 11/03/15.
 */
public class GameActivity extends Activity implements SampleApplicationControl, SensorEventListener {
    private static final String LOGTAG = "ImageTargets";

    private SampleApplicationSession vuforiaAppSession;
    private GestureDetector gestureDetector;

    private DataSet dataSet;

    // Our OpenGL view:
    private SampleApplicationGLView openGLview;

    // Our renderer:
    private ImageTargetRenderer renderer;

    // The textures we will use for rendering:
    private Vector<Texture> textures;

    //   Board Game classes
    private BoardModel boardModel;
    private PartieModel partieModel;

    private RelativeLayout layout;

    private LoadingDialogHandler loadingDialogHandler;

    private boolean isTouch;
    private Coordinate initialCoordonate;
    private float vitesseX, vitesseY, vitesseZ;
    private float[] gravity = new float[3];



    // Overlay widgets
    private TextView team1;
    private TextView team2;
    private RadioButton lp1;
    private RadioButton lp2;
    private RadioButton lp3;
    private RadioButton lp4;
    private RadioButton rp1;
    private RadioButton rp2;
    private RadioButton rp3;
    private RadioButton rp4;
    private Map<Integer,List<RadioButton>> mapRadioButton;
    private LinearLayout linearLayoutTeam1;
    private LinearLayout linearLayoutTeam2;

    private ImageView imagePalet;
    public LoadingDialogHandler getLoadingDialogHandler() {
        return loadingDialogHandler;
    }

    // Called when the activity first starts or the user navigates back to an
    // activity.
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.d(LOGTAG, "onCreate");
        super.onCreate(savedInstanceState);

        // set activity on fullscreen
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        vuforiaAppSession = new SampleApplicationSession(this);
        loadingDialogHandler = new LoadingDialogHandler(this);
        boardModel = new BoardModelImpl();
        partieModel = new PartieModelImpl(2,boardModel);
        gestureDetector = new GestureDetector(this, new GestureListener());

        // initialize sensor
        SensorManager senSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        Sensor senAccelerometer = senSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        senSensorManager.registerListener(this, senAccelerometer, SensorManager.SENSOR_DELAY_NORMAL);
        isTouch = false;
        initialCoordonate = null;

        // FIXME Temp add palets
        boardModel.addPalet(new CoordinateImpl(0, 0, 0), PaletModelImpl.createMasterPalet());


        startLoadingAnimation();

        vuforiaAppSession
                .initAR(this, ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        // Load any sample specific textures:
        textures = new Vector<Texture>();
        loadTextures();

    }

    // We want to load specific textures from the APK, which we will later use
    // for rendering.
    private void loadTextures() {
        textures.add(Texture.loadTextureFromApk("Texture/TextureBoard.png",
                getAssets()));
        textures.add(Texture.loadTextureFromApk("Texture/TexturePaletM.png",
                getAssets()));
        textures.add(Texture.loadTextureFromApk("Texture/TexturePalet1.png",
                getAssets()));
        textures.add(Texture.loadTextureFromApk("Texture/TexturePalet2.png",
                getAssets()));
    }


    // Called when the activity will start interacting with the user.
    @Override
    protected void onResume() {
        Log.d(LOGTAG, "onResume");
        super.onResume();

        try {
            vuforiaAppSession.resumeAR();
        } catch (SampleApplicationException e) {
            Log.e(LOGTAG, e.getString());
        }

        // Resume the GL view:
        if (openGLview != null) {
            openGLview.setVisibility(View.VISIBLE);
            openGLview.onResume();
        }

    }


    // Callback for configuration changes the activity handles itself
    @Override
    public void onConfigurationChanged(Configuration config) {
        Log.d(LOGTAG, "onConfigurationChanged");
        super.onConfigurationChanged(config);

        vuforiaAppSession.onConfigurationChanged();
    }


    // Called when the system is about to start resuming a previous activity.
    @Override
    protected void onPause() {
        Log.d(LOGTAG, "onPause");
        super.onPause();

        if (openGLview != null) {
            openGLview.setVisibility(View.INVISIBLE);
            openGLview.onPause();
        }

        try {
            vuforiaAppSession.pauseAR();
        } catch (SampleApplicationException e) {
            Log.e(LOGTAG, e.getString());
        }
    }


    // The final call you receive before your activity is destroyed.
    @Override
    protected void onDestroy() {
        Log.d(LOGTAG, "onDestroy");
        super.onDestroy();

        try {
            vuforiaAppSession.stopAR();
        } catch (SampleApplicationException e) {
            Log.e(LOGTAG, e.getString());
        }

        // Unload texture:
        textures.clear();
        textures = null;

        System.gc();
    }


    // Initializes AR application components.
    private void initApplicationAR() {
        // Create OpenGL ES view:
        int depthSize = 16;
        int stencilSize = 0;
        boolean translucent = Vuforia.requiresAlpha();

        openGLview = new SampleApplicationGLView(this);
        openGLview.init(translucent, depthSize, stencilSize);

        renderer = new ImageTargetRenderer(this, vuforiaAppSession, boardModel);
        renderer.setTextures(textures);
        openGLview.setRenderer(renderer);

    }


    private void startLoadingAnimation() {
        LayoutInflater inflater = LayoutInflater.from(this);
        layout = (RelativeLayout) inflater.inflate(R.layout.camera_overlay,
                null, false);
//        imagePalet = (ImageView)layout.findViewById(R.id.imageView1);
        mapRadioButton = new HashMap<Integer,List<RadioButton>>() ;
        team1 = (TextView)layout.findViewById(R.id.team1);
        team2 = (TextView)layout.findViewById(R.id.team2);
        rp1 = (RadioButton)layout.findViewById(R.id.rp1);
        rp2 = (RadioButton)layout.findViewById(R.id.rp2);
        rp3 = (RadioButton)layout.findViewById(R.id.rp3);
        rp4 = (RadioButton)layout.findViewById(R.id.rp4);
        lp1 = (RadioButton)layout.findViewById(R.id.lp1);
        lp2 = (RadioButton)layout.findViewById(R.id.lp2);
        lp3 = (RadioButton)layout.findViewById(R.id.lp3);
        lp4 = (RadioButton)layout.findViewById(R.id.lp4);
        List<RadioButton> listLP = new ArrayList<RadioButton>();
        List<RadioButton> listRP = new ArrayList<RadioButton>();
        listLP.add(lp1);
        listLP.add(lp2);
        listLP.add(lp3);
        listLP.add(lp4);

        listRP.add(rp1);
        listRP.add(rp2);
        listRP.add(rp3);
        listRP.add(rp4);

        mapRadioButton.put(0,listLP);
        mapRadioButton.put(1,listRP);


        linearLayoutTeam1 = (LinearLayout)layout.findViewById(R.id.leftlayout);
        linearLayoutTeam2 = (LinearLayout)layout.findViewById(R.id.rightlayout);
        linearLayoutTeam1.setBackgroundResource(R.color.green);

        // game initialization

        imagePalet = switchTeam(partieModel.getCurTeam());

        layout.setVisibility(View.VISIBLE);
        layout.setBackgroundColor(Color.BLACK);

        // Gets a reference to the loading dialog
        loadingDialogHandler.mLoadingDialogContainer = layout
                .findViewById(R.id.loading_indicator);

        // Shows the loading indicator at start
        loadingDialogHandler
                .sendEmptyMessage(LoadingDialogHandler.SHOW_LOADING_DIALOG);

        // Adds the inflated layout to the view
        addContentView(layout, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT));

    }


    // Methods to load and destroy tracking data.
    @Override
    public boolean doLoadTrackersData() {
        TrackerManager tManager = TrackerManager.getInstance();
        ImageTracker imageTracker = (ImageTracker) tManager
                .getTracker(ImageTracker.getClassType());
        if (imageTracker == null)
            return false;

        if (dataSet == null)
            dataSet = imageTracker.createDataSet();

        if (dataSet == null)
            return false;

        boolean loaded = dataSet.load("Board.xml", STORAGE_TYPE.STORAGE_APPRESOURCE);
        if (!loaded)
            return false;

        if (!imageTracker.activateDataSet(dataSet))
            return false;

        int numTrackables = dataSet.getNumTrackables();
        for (int count = 0; count < numTrackables; count++) {
            Trackable trackable = dataSet.getTrackable(count);

            String name = "Current Dataset : " + trackable.getName();
            trackable.setUserData(name);
            Log.d(LOGTAG, "UserData:Set the following user data "
                    + (String) trackable.getUserData());
        }

        return true;
    }


    @Override
    public boolean doUnloadTrackersData() {
        // Indicate if the trackers were unloaded correctly
        boolean result = true;

        TrackerManager tManager = TrackerManager.getInstance();
        ImageTracker imageTracker = (ImageTracker) tManager
                .getTracker(ImageTracker.getClassType());
        if (imageTracker == null)
            return false;

        if (dataSet != null && dataSet.isActive()) {
            if (imageTracker.getActiveDataSet().equals(dataSet)
                    && !imageTracker.deactivateDataSet(dataSet)) {
                result = false;
            } else if (!imageTracker.destroyDataSet(dataSet)) {
                result = false;
            }

            dataSet = null;
        }

        return result;
    }


    @Override
    public void onInitARDone(SampleApplicationException exception) {

        if (exception == null) {
            initApplicationAR();

            renderer.isActive = true;

            // Now add the GL surface view. It is important
            // that the OpenGL ES surface view gets added
            // BEFORE the camera is started and video
            // background is configured.
            addContentView(openGLview, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.MATCH_PARENT));

            // Sets the UILayout to be drawn in front of the camera
            layout.bringToFront();

            // Sets the layout background to transparent
            layout.setBackgroundColor(Color.TRANSPARENT);




            try {
                vuforiaAppSession.startAR(CameraDevice.CAMERA.CAMERA_DEFAULT);
            } catch (SampleApplicationException e) {
                Log.e(LOGTAG, e.getString());
            }

            CameraDevice.getInstance().setFocusMode(
                    CameraDevice.FOCUS_MODE.FOCUS_MODE_NORMAL);
        } else {
            Log.e(LOGTAG, exception.getString());
            finish();
        }
    }

    @Override
    public void onQCARUpdate(State state) {
    }


    @Override
    public boolean doInitTrackers() {
        // Indicate if the trackers were initialized correctly
        boolean result = true;

        TrackerManager tManager = TrackerManager.getInstance();
        Tracker tracker;

        // Trying to initialize the image tracker
        tracker = tManager.initTracker(ImageTracker.getClassType());
        if (tracker == null) {
            Log.e(
                    LOGTAG,
                    "Tracker not initialized. Tracker already initialized or the camera is already started");
            result = false;
        } else {
            Log.i(LOGTAG, "Tracker successfully initialized");
        }
        return result;
    }


    @Override
    public boolean doStartTrackers() {
        // Indicate if the trackers were started correctly
        boolean result = true;

        Tracker imageTracker = TrackerManager.getInstance().getTracker(
                ImageTracker.getClassType());
        if (imageTracker != null)
            imageTracker.start();

        return result;
    }


    @Override
    public boolean doStopTrackers() {
        // Indicate if the trackers were stopped correctly
        boolean result = true;

        Tracker imageTracker = TrackerManager.getInstance().getTracker(
                ImageTracker.getClassType());
        if (imageTracker != null)
            imageTracker.stop();

        return result;
    }


    @Override
    public boolean doDeinitTrackers() {
        // Indicate if the trackers were deinitialized correctly
        boolean result = true;

        TrackerManager tManager = TrackerManager.getInstance();
        tManager.deinitTracker(ImageTracker.getClassType());

        return result;
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_UP) {
            imagePalet.setVisibility(View.INVISIBLE);
            if (initialCoordonate != null) {
                new LoadPaletAsync().execute();
            }
        }
        return gestureDetector.onTouchEvent(event);
    }

    @Override
    public void onSensorChanged(SensorEvent sensorEvent) {
        Sensor mySensor = sensorEvent.sensor;

        /*if (mySensor.getType() == Sensor.TYPE_ACCELEROMETER) {
            vitesseX = sensorEvent.values[0];
            vitesseY = sensorEvent.values[1];
            vitesseZ = sensorEvent.values[2];
        }*/

        final float alpha = 0.8f;

        // Isolate the force of gravity with the low-pass filter.
        if (mySensor.getType() == Sensor.TYPE_GRAVITY) {
            gravity[0] = alpha * gravity[0] + (1 - alpha) * sensorEvent.values[0];
            gravity[1] = alpha * gravity[1] + (1 - alpha) * sensorEvent.values[1];
            gravity[2] = alpha * gravity[2] + (1 - alpha) * sensorEvent.values[2];
        }

        // Remove the gravity contribution with the high-pass filter.
        if (mySensor.getType() == Sensor.TYPE_ACCELEROMETER) {
            vitesseX = sensorEvent.values[0] - gravity[0];
            vitesseY = sensorEvent.values[1] - gravity[1];
            vitesseZ = sensorEvent.values[2] - gravity[2];
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {
        // Not used
    }

    private class GestureListener extends SimpleOnGestureListener {
        @Override
        public boolean onDown(MotionEvent e) {
            imagePalet.setVisibility(View.VISIBLE);
            if (initialCoordonate == null) {
                initialCoordonate = new CoordinateImpl(vitesseX, vitesseY, vitesseZ);
            }
            return true;
        }

        @Override
        public boolean onSingleTapUp(MotionEvent e) {
            imagePalet.setVisibility(View.INVISIBLE);
            return true;
        }
    }

    private class LoadPaletAsync extends AsyncTask<Void, Integer, Boolean> {
        double resultX;
        double resultY;

        protected Boolean doInBackground(Void... params)
        {
            resultX = Curve.xForGround(vitesseX - initialCoordonate.getX()
                    , vitesseZ - initialCoordonate.getZ()
                    , 0.90);
            resultY = Curve.yByx(resultX, vitesseX, vitesseY);

            initialCoordonate = null;
            Log.i("doInBackground", "Calculate coordinates");





            return true;
        }


        protected void onPostExecute(Boolean result)
        {
            SampleApplicationException vuforiaException = null;
            int teamNumber = partieModel.getCurTeam();
            Log.i("onPostExecute", "Add palet");
            float x = (float)resultY * 50 ;
            float y = (float)resultX * 50 -(float)boardModel.getBoardDst() / 100 ;


                boardModel.addPalet(new CoordinateImpl(x, y, 0), PaletModelImpl.createPalet(teamNumber + 1));

            int tmp = partieModel.paletLaunched();
            if(tmp != -1) {
               // if (partieModel.getTeams().get(teamNumber).getNbPalet() != -1  ){
                try {
                    mapRadioButton.get(teamNumber).get(partieModel.getTeams().get(teamNumber).getNbPalet()).setChecked(true);

                } catch (Exception e) {
                    e.printStackTrace();
                }
                imagePalet = switchTeam(tmp);
                // }
            }
            else {
                Toast.makeText(getApplicationContext(), "Winner is team " + partieModel.getFirstTeam(),
                        Toast.LENGTH_SHORT).show();
                finish();
            }

                Log.d(LOGTAG, "LoadPaletAsync.onPostExecute: execution "
                        + (result ? "successful" : "failed"));

                if (!result) {
                    String logMessage = "Failed to load palet.";
                    // Error loading dataset
                    Log.e(LOGTAG, logMessage);
                    vuforiaException = new SampleApplicationException(
                            SampleApplicationException.LOADING_TRACKERS_FAILURE,
                            logMessage);
                } else {
                    // Hint to the virtual machine that it would be a good time to
                    // run the garbage collector:
                    //
                    // NOTE: This is only a hint. There is no guarantee that the
                    // garbage collector will actually be run.
                    System.gc();

                }
        }
    }

    private ImageView switchTeam(int i){

        switch (i) {
            case 0: linearLayoutTeam1.setBackgroundResource(R.color.green);
                    linearLayoutTeam2.setBackgroundResource(R.color.white);
                    return (ImageView) layout.findViewById(R.id.imageView1);
            case 1: linearLayoutTeam2.setBackgroundResource(R.color.red);
                    linearLayoutTeam1.setBackgroundResource(R.color.white);
                    return (ImageView) layout.findViewById(R.id.imageView2);
            default:return (ImageView) layout.findViewById(R.id.imageView2);
        }
    }
}


