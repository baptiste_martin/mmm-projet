package mmm.m2gla.istic.fr.palet.model.impl;

import java.util.ArrayList;

import mmm.m2gla.istic.fr.palet.model.BoardModel;
import mmm.m2gla.istic.fr.palet.model.PaletModel;
import mmm.m2gla.istic.fr.palet.model.PartieModel;
import mmm.m2gla.istic.fr.palet.model.TeamModel;

/**
 * Created by david on 19/03/15.
 */
public class PartieModelImpl implements PartieModel {
    private int                     nbTeams;
    private ArrayList<TeamModel>    teams;
    private BoardModel              curBoard;
    private int                     curTeam;
    private int                     curTurn;


    public PartieModelImpl(int nbTeams, BoardModel curBoard) {
        this.nbTeams = nbTeams;
        this.teams = new ArrayList<>();
        this.curBoard = curBoard;
        this.curTeam = 0;
        this.curTurn = 0;

        for (int i = 0; i< this.nbTeams; i++) {
            this.teams.add(new TeamModelImpl(i));
        }
    }

    public ArrayList<TeamModel> getTeams() {
        return teams;
    }

    @Override
    public int  getNbTeams() {
        return this.nbTeams;
    }

    @Override
    public int getCurTeam() {
        return curTeam;
    }

    @Override
    public int  getTeamPoint(int teamNum) {
        int                     team;
        ArrayList<PaletModel>   paletList;


        if ((teamNum < 0) || (teamNum >= this.nbTeams)) {
            return (-1);
        }

        return(this.teams.get(teamNum).getPoint());
    }


    @Override
    public void  setPointToWinTeam() {
        int                     team;
        ArrayList<PaletModel>   paletList;


        paletList = this.curBoard.getAllPalets();

        team = -1;
        for (PaletModel pm:paletList) {
            if (pm.isMasterPalet()) {
                continue;
            }

            if (team == -1) {
                team = pm.getTeam();
            }
            else if (team != pm.getTeam()) {
                break;
            }
            if (team == -1) {

            }

            this.teams.get(team).addPoint(1);
        }
    }

    @Override
    public int  getFirstTeam() {
        ArrayList<PaletModel>   paletList;


        paletList = this.curBoard.getAllPalets();

        for (PaletModel pm:paletList) {
            if (pm.isMasterPalet()) {
                continue;
            }

            return (pm.getTeam());
        }

        return (-1);
    }

    @Override
    public int paletLaunched () {
        int team;


        if (this.teams.get(this.curTeam).usedPalet() == false) {
            return (-1);
        }

        if (this.curTurn == 0) {
            if (this.teams.get(this.curTeam).getNbPalet() == 0) {
                return (-1);
            }
            this.curTurn++;
            return this.curTeam;
        }

        this.curTurn = 0;

        team = this.getNextTeam(this.curTeam);
        if (team == -1) {
            return (-1);
        }

        if (this.getFirstTeam() == team) {
            team = this.getNextTeam(team);
            if (team == -1) {
                return (-1);
            }
        }

        this.curTeam = team;

        return team;
    }

    private int getNextTeam (int prevTeam) {
        int team = prevTeam;

        do {
            team++;
            if (team >= this.nbTeams) {
                team = 0;
            }

            if (team == prevTeam) {
                if (this.teams.get(team).getNbPalet() == 0) {
                    return (-1);
                }
                break;
            }
        }
        while (this.teams.get(team).getNbPalet() == 0);

        return (team);
    }
}
