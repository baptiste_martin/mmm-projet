package mmm.m2gla.istic.fr.palet.model.utils;

import java.util.ArrayList;

/**
 * Created by david on 11/03/15
 * Utilitaires de calcul.
 */
public class Curve {
    final static    int         coefM = 1000;   // Coeficient de division du mettre (1000 pour milimettre)
    final static    double      coefX = 0.1;    // Coeficient de discretion sur X pour l'obtention des z
    final static    double      g = 9.81;       // Acceleration de la gravite


    /*
        vx : Vitesse initiale en X
        vz : Vitesse initiale en Z
        hl : Hauteur de lancement en metres
     */
    public static double xForGround(double vx, double vz, double hl) {
        boolean         dico;
        double          z, xidx, xlower, xupper;
        int             zi, h = (int) (hl * coefM);


        xlower = 0;
        xupper = 5;
        xidx = 0;
        zi = 0;
        dico = false;
        while (zi != -h) {
            z = zByx(xidx, vx, vz);

            zi = (int) (coefM * z);
            if (zi > -h) {
                xlower = xidx;
                if (dico == false) {
                    xidx += xupper;
                }
                else {
                    xidx = (xupper + xidx)/ 2;
                }

            }
            else if (zi < -h) {
                xupper = xidx;
                xidx = (xlower + xidx) / 2;
                dico = true;
            }
        }

        return(xidx);
    }

    /*
        Methode fournissant une liste de point. Cette liste contient :
            La suite de points de la courbe
        vx : Vitesse initiale en X
        vy : Vitesse initiale en Y
        vz : Vitesse initiale en Z
        xf : X final
     */
    public ArrayList<SpatialRep> getCurveDotList(double vx, double vy, double vz, double xf) {
        ArrayList<SpatialRep>   dotList;
        SpatialRep              sp;
        double                  xidx;


        // Preparation de la liste de points
        dotList = new ArrayList<SpatialRep>();

        // Determination de la liste de points
        for (xidx = 0.0; xidx <= xf; xidx += coefX) {
            // Point courant
            sp = new SpatialRep();
            sp.x = xidx;
            sp.y = yByx(xidx, vx, vy);
            sp.z = zByx(xidx, vx, vz);
            sp.t = tByx(xidx, vx);

            // Ajout du point courant a la liste
            dotList.add(sp);
        }

        // Dernier Point
        sp = new SpatialRep();
        sp.x = xidx;
        sp.y = yByx(xidx, vx, vy);
        sp.z = zByx(xf, vx, vz);
        sp.t = tByx(xidx, vx);

        // Ajout du dernier point a la liste
        dotList.add(sp);


        return(dotList);
    }


    /*
        Methode fournissant le point precedent le dernier point d'une liste de points representant une courbe
        dotList : Liste de points
     */
    public SpatialRep getPreLastPointInList(ArrayList<SpatialRep> dotList) {
        SpatialRep              plp = null;


        // Preparation du point
        if (dotList.size() >= 2) {
            plp = dotList.get(dotList.size() - 2);
        }
        else if (dotList.size() == 1) {
            plp = dotList.get(0);
        }
        else {
            plp = new SpatialRep();
        }

        return (plp);
    }


    /*
        Methode fournissant le point haut d'une liste de points representant une courbe
        dotList : Liste de points
     */
    public SpatialRep getHighPointInList(ArrayList<SpatialRep> dotList) {
        SpatialRep              hp;


        // Preparation du point haut
        hp = new SpatialRep();

        // Determination du point haut
        for (SpatialRep sp: dotList) {
            if (hp.z < sp.z) {
                hp.x = sp.x;
                hp.y = sp.y;
                hp.z = sp.z;
            }
        }

        return (hp);
    }


    public static double zByx(double x, double vx, double vz) {
        double  z;


        z = - 0.5 * (g / Math.pow(vx, 2)) * Math.pow(x, 2);
        z += (vz/vx) * x;

        return z;
    }

    public static double yByx(double x, double vx, double vy) {
        double  y, t;


        t = tByx(x, vx);
        y = vy * t;

        return y;
    }

    public static double tByx(double x, double vx) {
        double  t;


        t = x / vx;

        return t;
    }

    public double getAngle(double xori, double yori, double xdst, double ydst) {
        double  x, y;
        double  angle, tan;


        x = xdst - xori;
        y = ydst - yori;
        if (x == 0) {
            if (y < 0) {
                return (- (Math.PI / 2));
            }
            return (Math.PI / 2);
        }

        tan = y / x;

        angle = Math.atan(tan);

        return angle;
    }

    public double convertInDegre(double angle) {
        return(angle * 180 / Math.PI);
    }

    public class SpatialRep {
        public SpatialRep(double x, double y, double z, double t) {
            this.x = x;
            this.y = y;
            this.z = z;
            this.t = t;
        }

        public SpatialRep() {
            this.x = 0;
            this.y = 0;
            this.z = 0;
            this.t = 0;
        }

        public double x;
        public double y;
        public double z;
        public double t;
    }


    /*
        Methodes permettant d'initialiser une reference de point
     */
    public SpatialRep getNewSpatialRep() {
        return new SpatialRep();
    }

    public SpatialRep getNewSpatialRep(double x, double y, double z, double t) {
        return new SpatialRep(x, y, z, t);
    }


    /*
        Methode affichant tous les points d'une courbe
        dotList : Liste de points
     */
    public void dispDotList(ArrayList<SpatialRep> dotList) {
        // Determination du point haut
        for (SpatialRep sp: dotList) {
            System.out.println("X = " + sp.x + "  Y = " + sp.y + "  Z = " + sp.z + "  T = " + sp.t);
        }

    }


/*
    public double getAngle(double xori, double yori, double xdst, double ydst) {
        double  angle, lx2, ly2, d, r, x;

        lx2 = Math.pow((xdst - xori), 2);
        ly2 = Math.pow((ydst - yori), 2);

        d = Math.sqrt(lx2 + ly2);
        if (d == 0) {
            return(0);
        }

        r = 1 / d;
        x = (xdst - xori) * r;

        return angle;
    }
*/
}

