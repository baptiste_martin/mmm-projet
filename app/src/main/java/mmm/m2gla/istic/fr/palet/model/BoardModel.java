package mmm.m2gla.istic.fr.palet.model;

import java.util.ArrayList;
import java.util.Set;

import mmm.m2gla.istic.fr.palet.SampleApplication.objects.Palet;
import mmm.m2gla.istic.fr.palet.model.Coordinate;

/**
 * Created by baptiste on 18/03/15.
 */
public interface BoardModel {
    public static final int BOARD_WIDTH = 700;
    public static final int BOARD_HEIGHT = 700;
    public static final int BOARD_DEPTH = 30;
    public static final int DFTDST = 5500;
    public static final int MILINMETER = 1000;


    public boolean addPalet(Coordinate coordinate, PaletModel palet);
    public ArrayList<PaletModel> getPalets(Coordinate coordinate);
    public ArrayList<PaletModel> getAllPalets();
    public SquareModel getBoardSquare(Coordinate coordinate);
    public boolean isInBoard(Coordinate coordinate);
    public PaletModel getMaster();
    public int getxNbSquares();
    public int getyNbSquares();
    public int getBoardDst();
}
