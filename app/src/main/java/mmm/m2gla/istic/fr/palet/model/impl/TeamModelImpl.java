package mmm.m2gla.istic.fr.palet.model.impl;

import mmm.m2gla.istic.fr.palet.model.TeamModel;

/**
 * Created by david on 19/03/15.
 */
public class TeamModelImpl implements TeamModel {
    private int teamNum;
    private int point;
    private int nbPalet;


    public TeamModelImpl(int teamNum) {
        this.teamNum = teamNum;
        this.point = 0;
        this.nbPalet = TeamModel.DEFAULT_NB_PALETS_IN_PARTIE;
    }

    @Override
    public void addPoint(int point) {
        this.point += point;
    }

    @Override
    public int getTeamNum() {
        return teamNum;
    }

    @Override
    public int getPoint() {
        return point;
    }

    @Override
    public int getNbPalet() {
        return nbPalet;
    }

    @Override
    public boolean usedPalet() {
        if (this.nbPalet <= 0) {
            return (false);
        }
        this.nbPalet--;

        return (true);
    }
}
