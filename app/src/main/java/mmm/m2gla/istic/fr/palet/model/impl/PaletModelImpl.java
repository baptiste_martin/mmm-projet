package mmm.m2gla.istic.fr.palet.model.impl;

import mmm.m2gla.istic.fr.palet.model.Coordinate;
import mmm.m2gla.istic.fr.palet.model.PaletModel;
import mmm.m2gla.istic.fr.palet.model.SquareModel;

/**
 * Created by baptiste on 18/03/15.
 */
public class PaletModelImpl implements PaletModel {
    private SquareModel     currentSquare;              // Case principale occupee sur le plateau
                                                        // Case occupee par le centre du palet
    private int             currentHeight;              // Hauteur sur cette case
                                                        //  -1 : Non lancé
                                                        //  0  : Hors du plateau
                                                        //  1  : Plus bas sur le plateau
    private boolean         masterPalet;                // true si le palet est le petit
    private float           scaleXY;
    private int             team;                       // Equipe du palet
    private Coordinate      coordinates;                // Coordonnées du palet


    private PaletModelImpl() {
        this.scaleXY = SCALE_X_Y_MASTER;
        this.masterPalet = true;
        this.team = -1;
    }

    private PaletModelImpl(int team) {
        this.scaleXY = SCALE_X_Y;
        this.masterPalet = false;
        this.team = team;
    }

    @Override
    public float getScaleXY() {
        return scaleXY;
    }

    @Override
    public int getTeam() {
        return team;
    }

    @Override
    public SquareModel getCurrentSquare() {
        return currentSquare;
    }

    @Override
    public Coordinate getCoordinates() {
        return coordinates;
    }

    @Override
    public int getCurrentHeight() {
        return currentHeight;
    }

    @Override
    public boolean isMasterPalet() {
        return masterPalet;
    }

    @Override
    public void setCurrentSquare(SquareModel currentSquare) {
        this.currentSquare = currentSquare;
    }

    @Override
    public void setCoordinates(Coordinate coordinates) {
        this.coordinates = coordinates;
    }

    @Override
    public void setCurrentHeight(int currentHeight) {
        this.currentHeight = currentHeight;
    }

    public static PaletModel createMasterPalet() {
        return new PaletModelImpl();
    }

    public static PaletModel createPalet(int team) {
        return new PaletModelImpl(team);
    }

    @Override
    public int compareTo(Object another) {
        float cdx, cdy, odx, ody, cdm, odm;
        PaletModel  anotherPM = (PaletModel) another;
        PaletModel  master;


        if (anotherPM.getCurrentSquare() == null) {
            return (-1);
        }
        if (this.currentSquare == null) {
            return (1);
        }
        if (this.isMasterPalet()) {
            return (-1);
        }
        if (anotherPM.isMasterPalet()) {
            return (1);
        }

        master = this.currentSquare.getBoardRef().getMaster();
        if (master == null) {
            return (0);
        }

        cdx = Math.abs(this.coordinates.getX() - master.getCoordinates().getX());
        cdy = Math.abs(this.coordinates.getY() - master.getCoordinates().getY());
        odx = Math.abs(anotherPM.getCoordinates().getX() - master.getCoordinates().getX());
        ody = Math.abs(anotherPM.getCoordinates().getY() - master.getCoordinates().getY());

        cdm = (float) (Math.pow(cdx, 2.0) + Math.pow(cdy, 2.0));
        odm = (float) (Math.pow(odx, 2.0) + Math.pow(ody, 2.0));

        if (cdm < odm) {
            return (-1);
        }
        if (cdm > odm) {
            return (1);
        }

        if (this.currentHeight < anotherPM.getCurrentHeight()) {
            return (1);
        }
        if (this.currentHeight > anotherPM.getCurrentHeight()) {
            return (-1);
        }

        return 0;
    }
}
