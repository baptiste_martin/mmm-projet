package mmm.m2gla.istic.fr.palet.model;

/**
 * Created by baptiste on 18/03/15.
 */
public interface Coordinate {
    public float getX();
    public void setX(float x);
    public float getY();
    public void setY(float y);
    public float getZ();
    public void setZ(float z);
}
