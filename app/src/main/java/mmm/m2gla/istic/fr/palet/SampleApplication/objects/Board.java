package mmm.m2gla.istic.fr.palet.SampleApplication.objects;

import java.nio.Buffer;

import mmm.m2gla.istic.fr.palet.SampleApplication.utils.MeshObject;

public class Board extends MeshObject {
    private Buffer verticesBuffer;
    private Buffer texCoordsBuffer;
    private Buffer normalsBuffer;
    private Buffer indicesBuffer;

    private int indicesNumber = 0;
    private int verticesNumber = 0;

    public Board() {
        setVertices();
        setCubeTexCoords();
        setCubeNormals();
        setCubeIndices();
    }

    private void setVertices() {
        double[] cubeVertices = {
                63.999968f, 63.999968f, -63.999968f, 63.999968f, -63.999968f, -63.999968f, -63.999968f, -63.999968f, -63.999968f, -63.999968f, 63.999968f, -63.999968f, 63.999968f, 63.999904f, 63.999968f, -63.999968f, 63.999968f, 63.999968f, -63.999968f, -63.999968f, 63.999968f, 63.999904f, -64.000032f, 63.999968f, 63.999968f, 63.999968f, -63.999968f, 63.999968f, 63.999904f, 63.999968f, 63.999904f, -64.000032f, 63.999968f, 63.999968f, -63.999968f, -63.999968f, 63.999968f, -63.999968f, -63.999968f, 63.999904f, -64.000032f, 63.999968f, -63.999968f, -63.999968f, 63.999968f, -63.999968f, -63.999968f, -63.999968f, -63.999968f, -63.999968f, -63.999968f, -63.999968f, -63.999968f, 63.999968f, -63.999968f, 63.999968f, 63.999968f, -63.999968f, 63.999968f, -63.999968f, 63.999968f, 63.999904f, 63.999968f, 63.999968f, 63.999968f, -63.999968f, -63.999968f, 63.999968f, -63.999968f, -63.999968f, 63.999968f, 63.999968f,
        };

        verticesBuffer = fillBuffer(cubeVertices);
        verticesNumber = cubeVertices.length / 3;
    }

    private void setCubeTexCoords() {
        double[] cubeTexCoords = {
                0.666667f, 0.000000f, 1.000000f, 0.000000f, 1.000000f, 0.333333f, 0.666667f, 0.333333f, 0.000000f, 0.666667f, 0.000000f, 0.333333f, 0.333333f, 0.333334f, 0.333333f, 0.666667f, 0.666667f, 0.333333f, 0.333333f, 0.333333f, 0.333333f, 0.000000f, 0.666667f, 0.000000f, 0.000000f, 0.333333f, 0.000000f, 0.000000f, 0.333333f, 0.000000f, 0.333333f, 0.333333f, 0.666667f, 0.333333f, 1.000000f, 0.333333f, 1.000000f, 0.666667f, 0.666667f, 0.666667f, 0.333333f, 0.333333f, 0.666667f, 0.333333f, 0.666667f, 0.666667f, 0.333333f, 0.666667f,
        };

        texCoordsBuffer = fillBuffer(cubeTexCoords);
    }

    private void setCubeNormals() {
        double[] cubeNormals = {
                0.000000f, 0.000000f, -1.000000f, 0.000000f, 0.000000f, -1.000000f, 0.000000f, 0.000000f, -1.000000f, 0.000000f, 0.000000f, -1.000000f, 0.000000f, -0.000000f, 1.000000f, 0.000000f, -0.000000f, 1.000000f, 0.000000f, -0.000000f, 1.000000f, 0.000000f, -0.000000f, 1.000000f, 1.000000f, -0.000000f, 0.000000f, 1.000000f, -0.000000f, 0.000000f, 1.000000f, -0.000000f, 0.000000f, 1.000000f, -0.000000f, 0.000000f, -0.000000f, -1.000000f, -0.000000f, -0.000000f, -1.000000f, -0.000000f, -0.000000f, -1.000000f, -0.000000f, -0.000000f, -1.000000f, -0.000000f, -1.000000f, 0.000000f, -0.000000f, -1.000000f, 0.000000f, -0.000000f, -1.000000f, 0.000000f, -0.000000f, -1.000000f, 0.000000f, -0.000000f, 0.000000f, 1.000000f, 0.000000f, 0.000000f, 1.000000f, 0.000000f, 0.000000f, 1.000000f, 0.000000f, 0.000000f, 1.000000f, 0.000000f,
        };

        normalsBuffer = fillBuffer(cubeNormals);
    }

    private void setCubeIndices() {
        short[] cubeIndices = {
                0, 1, 2, 0, 2, 3, 4, 5, 6, 4, 6, 7, 8, 9, 10, 8, 10, 11, 12, 13, 14, 12, 14, 15, 16, 17, 18, 16, 18, 19, 20, 21, 22, 20, 22, 23,
        };

        indicesBuffer = fillBuffer(cubeIndices);
        indicesNumber = cubeIndices.length;
    }

    @Override
    public Buffer getBuffer(BUFFER_TYPE bufferType) {
        switch (bufferType) {
            case BUFFER_TYPE_VERTEX :
                return verticesBuffer;
            case BUFFER_TYPE_TEXTURE_COORD :
                return texCoordsBuffer;
            case BUFFER_TYPE_NORMALS :
               return normalsBuffer;
            case BUFFER_TYPE_INDICES :
                return indicesBuffer;
            default:
                return null;
        }
    }

    @Override
    public int getNumObjectVertex() {
        return verticesNumber;
    }

    @Override
    public int getNumObjectIndex() {
        return indicesNumber;
    }
}
