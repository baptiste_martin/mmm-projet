package mmm.m2gla.istic.fr.palet.model.impl;

import mmm.m2gla.istic.fr.palet.model.Coordinate;

/**
 * Created by baptiste on 18/03/15.
 * Coordonnées
 */
public class CoordinateImpl implements Coordinate {
    private float x;
    private float y;
    private float z;

    public CoordinateImpl(float x, float y, float z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public float getX() {
        return x;
    }

    public void setX(float x) {
        this.x = x;
    }

    public float getY() {
        return y;
    }

    public void setY(float y) {
        this.y = y;
    }

    public float getZ() {
        return z;
    }

    public void setZ(float z) {
        this.z = z;
    }
}
