package mmm.m2gla.istic.fr.palet.model;

/**
 * Created by david on 19/03/15.
 */
public interface TeamModel {
    public static final int DEFAULT_NB_PALETS_IN_PARTIE = 4;
    public static final int DEFAULT_NB_PALET_IN_PHASE = 2;


    public void addPoint(int point);
    public int getTeamNum();
    public int getPoint();
    public int getNbPalet();
    public boolean usedPalet();
}
