package mmm.m2gla.istic.fr.palet.model;

/**
 * Created by baptiste on 18/03/15.
 */
public interface PaletModel extends Comparable {
    public static final float SCALE_X_Y_MASTER = 0.25f;
    public static final float SCALE_X_Y = 0.4f;
    public static final int  GPALET_DIAMETER = 55;
    public static final int  PPALET_DIAMETER = 45;
    public static final int GPALET_DEPTH = 15;
    public static final int PPALET_DEPTH = 5;


    public float getScaleXY();
    public SquareModel getCurrentSquare();
    public Coordinate getCoordinates();
    public int getCurrentHeight();
    public boolean isMasterPalet();
    public int getTeam();
    public void setCurrentSquare(SquareModel currentSquare);
    public void setCoordinates(Coordinate coordinates);
    public void setCurrentHeight(int currentHeight);
}
