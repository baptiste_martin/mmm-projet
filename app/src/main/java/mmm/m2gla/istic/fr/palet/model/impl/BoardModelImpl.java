package mmm.m2gla.istic.fr.palet.model.impl;

import android.util.Log;

import java.util.ArrayList;
import java.util.Collections;

import mmm.m2gla.istic.fr.palet.model.BoardModel;
import mmm.m2gla.istic.fr.palet.model.Coordinate;
import mmm.m2gla.istic.fr.palet.model.PaletModel;
import mmm.m2gla.istic.fr.palet.model.SquareModel;

/**
 * Created by david on 18/03/15.
 * Classe décrivant le plateau de jeu du palets
 */
public class BoardModelImpl implements BoardModel {
    private SquareModel squares[][];
    private int         xNbSquares;
    private int         yNbSquares;
    private PaletModel  master;

    private int         boardDst;


    public BoardModelImpl() {
        this.xNbSquares = BoardModel.BOARD_WIDTH / PaletModel.GPALET_DIAMETER;
        this.yNbSquares = BoardModel.BOARD_HEIGHT / PaletModel.GPALET_DIAMETER;
        this.squares = new SquareModel[this.xNbSquares][this.yNbSquares];
        this.master = null;
        this.boardDst = BoardModel.DFTDST;

        for (int i = 0; i < this.xNbSquares; i++) {
            for (int j = 0; j < this.yNbSquares; j++) {
                this.squares[i][j] = new SquareModelImpl(i * PaletModel.GPALET_DIAMETER - BoardModel.BOARD_WIDTH / 2, j * PaletModel.GPALET_DIAMETER - BoardModel.BOARD_HEIGHT / 2, this);
            }
        }
    }

    /*
     * La distance doit etre exprimee en metres
     */
    public BoardModelImpl(float dst) {
        this();
        this.boardDst = (int) (dst * BoardModel.MILINMETER);
    }

    @Override
    public boolean addPalet(Coordinate coordinate, PaletModel palet) {
        SquareModel curSquare;

        curSquare = getBoardSquare(coordinate);
        if (curSquare == null) {
            Log.e("addPalet", "Palet out of board (x, y): "+ coordinate.getX() + ", "+ coordinate.getY());
            //return false;
        } else {
            curSquare.addPalet(palet);
            palet.setCurrentSquare(curSquare);
            // la coordonée en Z depend du nombre de palets superposés et de l'épaisseur de la planche
            coordinate.setZ(curSquare.getNbPalet() * PaletModel.GPALET_DEPTH + BOARD_DEPTH);
            palet.setCurrentHeight(curSquare.getNbPalet());
        }

        palet.setCoordinates(coordinate);

        if (palet.isMasterPalet()) {
            this.master = palet;
        }

        return true;
    }

    @Override
    public ArrayList<PaletModel> getAllPalets() {
        ArrayList<PaletModel>   palets;


        palets = new ArrayList<>();
        for (int i = 0; i < this.xNbSquares; i++) {
            for (int j = 0; j < this.yNbSquares; j++) {
                palets.addAll(this.squares[i][j].getPaletsList());
            }
        }

        Collections.sort(palets);
        return palets;
    }

    @Override
    public ArrayList<PaletModel> getPalets(Coordinate coordinate) {
        ArrayList<PaletModel>   palets;
        SquareModel             curSquare;


        curSquare = getBoardSquare(coordinate);
        if (curSquare == null) {
            palets = new ArrayList<>();
        }
        else {
            palets = curSquare.getPaletsList();
        }

        return palets;
    }

    @Override
    public SquareModel getBoardSquare(Coordinate coordinate) {
        int i = (int)((coordinate.getX() + BoardModel.BOARD_WIDTH / 2f ) / PaletModel.GPALET_DIAMETER);
        int j = (int)((coordinate.getY() + BoardModel.BOARD_HEIGHT / 2f ) / PaletModel.GPALET_DIAMETER);


        if ((i < 0) || (i >= this.xNbSquares) || (j < 0) || (j >= this.yNbSquares)) {
            Log.e("getBoardSquare", "Palet out of board (x, y) : "+ coordinate.getX() + ", "+ coordinate.getY() + " : " + i + ", " + j);
            return null;
        }else{
            Log.i("getBoardSquare", "get square at "+ coordinate.getX() + ", "+ coordinate.getY() + " : " + i + ", " + j);
            return (this.squares[i][j]);
        }
    }

    @Override
    public boolean isInBoard(Coordinate coordinate) {
        return (getBoardSquare(coordinate)!=null);
    }

    @Override
    public PaletModel getMaster() {
        return master;
    }

    @Override
    public int getxNbSquares() {
        return xNbSquares;
    }

    @Override
    public int getyNbSquares() {
        return yNbSquares;
    }

    @Override
    public int getBoardDst() {
        return boardDst;
    }
}
