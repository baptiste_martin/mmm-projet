package mmm.m2gla.istic.fr.palet.model;

import java.util.ArrayList;

/**
 * Created by david on 18/03/15.
 */
public interface SquareModel extends Coordinate {
    public ArrayList<PaletModel>    getPaletsList();
    public void                     addPalet(PaletModel newPalet);
    public int                      getNbPalet();
    public BoardModel               getBoardRef();
}
