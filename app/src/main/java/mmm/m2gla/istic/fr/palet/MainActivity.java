package mmm.m2gla.istic.fr.palet;

import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.Toast;

public class MainActivity extends ActionBarActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // set activity on fullscreen
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        Button buttonValider = (Button)findViewById(R.id.buttonStart);
        buttonValider.setOnClickListener(buttonStartClickListener);
    }

    private View.OnClickListener buttonStartClickListener = new View.OnClickListener() {



        @Override
        public void onClick(View view) {
            Toast.makeText(getApplicationContext(), "Jeu démarré", Toast.LENGTH_SHORT).show();

            Intent intent = new Intent(getApplicationContext(), GameActivity.class);
            startActivity(intent);
        }
    };
}
