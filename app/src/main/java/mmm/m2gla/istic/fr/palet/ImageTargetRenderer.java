package mmm.m2gla.istic.fr.palet;

import android.opengl.GLES20;
import android.opengl.GLSurfaceView;
import android.opengl.Matrix;
import android.util.Log;

import com.qualcomm.vuforia.Matrix44F;
import com.qualcomm.vuforia.Renderer;
import com.qualcomm.vuforia.State;
import com.qualcomm.vuforia.Tool;
import com.qualcomm.vuforia.Trackable;
import com.qualcomm.vuforia.TrackableResult;
import com.qualcomm.vuforia.VIDEO_BACKGROUND_REFLECTION;
import com.qualcomm.vuforia.Vuforia;

import mmm.m2gla.istic.fr.palet.SampleApplication.objects.Board;
import mmm.m2gla.istic.fr.palet.SampleApplication.objects.Palet;
import mmm.m2gla.istic.fr.palet.SampleApplication.utils.CubeShaders;
import mmm.m2gla.istic.fr.palet.SampleApplication.utils.LoadingDialogHandler;
import mmm.m2gla.istic.fr.palet.SampleApplication.utils.SampleUtils;
import mmm.m2gla.istic.fr.palet.SampleApplication.utils.Texture;

import java.util.List;
import java.util.Vector;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

import mmm.m2gla.istic.fr.palet.SampleApplication.SampleApplicationSession;
import mmm.m2gla.istic.fr.palet.model.BoardModel;
import mmm.m2gla.istic.fr.palet.model.Coordinate;
import mmm.m2gla.istic.fr.palet.model.PaletModel;

// The renderer class for the ImageTargets sample. 
public class ImageTargetRenderer implements GLSurfaceView.Renderer {
    private static final String LOGTAG = "ImageTargetRenderer";

    private SampleApplicationSession session;
    private GameActivity activity;
    private BoardModel boardModel;

    private Vector<Texture> textures;

    private int shaderProgramID;

    private int vertexHandle;

    private int normalHandle;

    private int textureCoordHandle;

    private int mvpMatrixHandle;

    private int texSampler2DHandle;

    private Board board;

    private Renderer renderer;

    boolean isActive = false;

    private static final float BOARD_SCALE_X = 3.0f;
    private static final float BOARD_SCALE_Y = 3.0f;
    private static final float BOARD_SCALE_Z = 0.05f;

    private static final float PALET_SCALE_Z = 0.4f;

    public ImageTargetRenderer(GameActivity activity,
                               SampleApplicationSession session, BoardModel boardModel) {
        this.activity = activity;
        this.session = session;
        this.boardModel = boardModel;
    }


    // Called to draw the current frame.
    @Override
    public void onDrawFrame(GL10 gl) {
        if (!isActive)
            return;

        // Render game board and palet
        drawGame();
    }

    // Called when the surface is created or recreated.
    @Override
    public void onSurfaceCreated(GL10 gl, EGLConfig config) {
        Log.d(LOGTAG, "GLRenderer.onSurfaceCreated");

        initRendering();

        // Call Vuforia function to (re)initialize rendering after first use
        // or after OpenGL ES context was lost (e.g. after onPause/onResume):
        session.onSurfaceCreated();
    }


    // Called when the surface changed size.
    @Override
    public void onSurfaceChanged(GL10 gl, int width, int height) {
        Log.d(LOGTAG, "GLRenderer.onSurfaceChanged");

        // Call Vuforia function to handle render surface size changes:
        session.onSurfaceChanged(width, height);
    }

    // Function for initializing the renderer.
    private void initRendering() {
        board = new Board();

        renderer = Renderer.getInstance();

        GLES20.glClearColor(0.0f, 0.0f, 0.0f, Vuforia.requiresAlpha() ? 0.0f
                : 1.0f);

        for (Texture t : textures) {
            GLES20.glGenTextures(1, t.mTextureID, 0);
            GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, t.mTextureID[0]);
            GLES20.glTexParameterf(GLES20.GL_TEXTURE_2D,
                    GLES20.GL_TEXTURE_MIN_FILTER, GLES20.GL_LINEAR);
            GLES20.glTexParameterf(GLES20.GL_TEXTURE_2D,
                    GLES20.GL_TEXTURE_MAG_FILTER, GLES20.GL_LINEAR);
            GLES20.glTexImage2D(GLES20.GL_TEXTURE_2D, 0, GLES20.GL_RGBA,
                    t.mWidth, t.mHeight, 0, GLES20.GL_RGBA,
                    GLES20.GL_UNSIGNED_BYTE, t.mData);
        }

        // activate the shader program and bind the vertex/normal/tex coords
        GLES20.glUseProgram(shaderProgramID);

        shaderProgramID = SampleUtils.createProgramFromShaderSrc(
                CubeShaders.CUBE_MESH_VERTEX_SHADER,
                CubeShaders.CUBE_MESH_FRAGMENT_SHADER);

        vertexHandle = GLES20.glGetAttribLocation(shaderProgramID,
                "vertexPosition");
        normalHandle = GLES20.glGetAttribLocation(shaderProgramID,
                "vertexNormal");
        textureCoordHandle = GLES20.glGetAttribLocation(shaderProgramID,
                "vertexTexCoord");
        mvpMatrixHandle = GLES20.glGetUniformLocation(shaderProgramID,
                "modelViewProjectionMatrix");
        texSampler2DHandle = GLES20.glGetUniformLocation(shaderProgramID,
                "texSampler2D");

        // Hide the Loading Dialog
        activity.getLoadingDialogHandler().sendEmptyMessage(LoadingDialogHandler.HIDE_LOADING_DIALOG);

    }

    // The render function.
    private void drawGame() {
        GLES20.glClear(GLES20.GL_COLOR_BUFFER_BIT | GLES20.GL_DEPTH_BUFFER_BIT);

        State state = renderer.begin();
        renderer.drawVideoBackground();

        GLES20.glEnable(GLES20.GL_DEPTH_TEST);

        drawBoard(state);

        drawPalets(boardModel, state);

        GLES20.glDisable(GLES20.GL_DEPTH_TEST);

        renderer.end();
    }

    private void drawBoard(State state) {
        // handle face culling, we need to detect if we are using reflection
        // to determine the direction of the culling
        GLES20.glEnable(GLES20.GL_CULL_FACE);
        GLES20.glCullFace(GLES20.GL_BACK);
        GLES20.glFrontFace(GLES20.GL_CCW); // Back camera

        // did we find any trackables this frame?
        for (int tIdx = 0; tIdx < state.getNumTrackableResults(); tIdx++) {
            TrackableResult result = state.getTrackableResult(tIdx);
            Trackable trackable = result.getTrackable();
            printUserData(trackable);
            Matrix44F modelViewMatrix_Vuforia = Tool
                    .convertPose2GLMatrix(result.getPose());
            float[] modelViewMatrix = modelViewMatrix_Vuforia.getData();

            // deal with the modelview and projection matrices
            float[] modelViewProjection = new float[16];

            // activate the shader program and bind the vertex/normal/tex coords
            GLES20.glUseProgram(shaderProgramID);

            Matrix.translateM(modelViewMatrix, 0, 0.0f, 0.0f, 0.0f);
            Matrix.scaleM(modelViewMatrix, 0, BOARD_SCALE_X,
                    BOARD_SCALE_Y, BOARD_SCALE_Z);

            Matrix.multiplyMM(modelViewProjection, 0, session
                    .getProjectionMatrix().getData(), 0, modelViewMatrix, 0);

            GLES20.glVertexAttribPointer(vertexHandle, 3, GLES20.GL_FLOAT,
                    false, 0, board.getVertices());
            GLES20.glVertexAttribPointer(normalHandle, 3, GLES20.GL_FLOAT,
                    false, 0, board.getNormals());
            GLES20.glVertexAttribPointer(textureCoordHandle, 2,
                    GLES20.GL_FLOAT, false, 0, board.getTexCoords());

            GLES20.glEnableVertexAttribArray(vertexHandle);
            GLES20.glEnableVertexAttribArray(normalHandle);
            GLES20.glEnableVertexAttribArray(textureCoordHandle);

            // activate texture 0, bind it, and pass to shader
            GLES20.glActiveTexture(GLES20.GL_TEXTURE0);
            GLES20.glBindTexture(GLES20.GL_TEXTURE_2D,
                    textures.get(0).mTextureID[0]);
            GLES20.glUniform1i(texSampler2DHandle, 0);

            // pass the model view matrix to the shader
            GLES20.glUniformMatrix4fv(mvpMatrixHandle, 1, false,
                    modelViewProjection, 0);

            // finally draw the Board
            GLES20.glDrawElements(GLES20.GL_TRIANGLES,
                    board.getNumObjectIndex(), GLES20.GL_UNSIGNED_SHORT,
                    board.getIndices());

            // disable the enabled arrays
            GLES20.glDisableVertexAttribArray(vertexHandle);
            GLES20.glDisableVertexAttribArray(normalHandle);
            GLES20.glDisableVertexAttribArray(textureCoordHandle);

            SampleUtils.checkGLError("Render Frame");

        }
    }

    private void drawPalets(BoardModel boardModel, State state) {
            List<PaletModel> palets = boardModel.getAllPalets();
            // handle face culling, we need to detect if we are using reflection
            // to determine the direction of the culling
            GLES20.glEnable(GLES20.GL_CULL_FACE);
            GLES20.glCullFace(GLES20.GL_BACK);
            GLES20.glFrontFace(GLES20.GL_CCW); // Back camera

            for (int iPalet = 0; iPalet < palets.size(); iPalet++) {
                Palet palet = new Palet();
                // did we find any trackables this frame?
                for (int tIdx = 0; tIdx < state.getNumTrackableResults(); tIdx++) {
                    PaletModel paletModel = palets.get(iPalet);
                    TrackableResult result = state.getTrackableResult(tIdx);
                    Trackable trackable = result.getTrackable();
                    printUserData(trackable);
                    Matrix44F modelViewMatrix_Vuforia = Tool
                            .convertPose2GLMatrix(result.getPose());
                    float[] modelViewMatrix = modelViewMatrix_Vuforia.getData();

                    // deal with the modelview and projection matrices
                    float[] modelViewProjection = new float[16];
                    Matrix.translateM(modelViewMatrix, 0,
                           paletModel.getCoordinates().getX(),
                           paletModel.getCoordinates().getY(),
                           paletModel.getCoordinates().getZ());
                    Matrix.scaleM(modelViewMatrix, 0,paletModel.getScaleXY(),
                           paletModel.getScaleXY(), PALET_SCALE_Z);

                    Matrix.multiplyMM(modelViewProjection, 0, session
                            .getProjectionMatrix().getData(), 0, modelViewMatrix, 0);

                    GLES20.glVertexAttribPointer(vertexHandle, 3, GLES20.GL_FLOAT,
                            false, 0, palet.getVertices());
                    GLES20.glVertexAttribPointer(normalHandle, 3, GLES20.GL_FLOAT,
                            false, 0, palet.getNormals());
                    GLES20.glVertexAttribPointer(textureCoordHandle, 2,
                            GLES20.GL_FLOAT, false, 0, palet.getTexCoords());

                    GLES20.glEnableVertexAttribArray(vertexHandle);
                    GLES20.glEnableVertexAttribArray(normalHandle);
                    GLES20.glEnableVertexAttribArray(textureCoordHandle);

                    // activate texture 0, bind it, and pass to shader
                    GLES20.glActiveTexture(GLES20.GL_TEXTURE0);
                    GLES20.glBindTexture(GLES20.GL_TEXTURE_2D,
                            textures.get(paletModel.getTeam() != -1? paletModel.getTeam()+1 :1).mTextureID[0]);
                    GLES20.glUniform1i(texSampler2DHandle, 0);

                    // pass the model view matrix to the shader
                    GLES20.glUniformMatrix4fv(mvpMatrixHandle, 1, false,
                            modelViewProjection, 0);

                    // finally draw the palet
                    GLES20.glDrawElements(GLES20.GL_TRIANGLES,
                            palet.getNumObjectIndex(), GLES20.GL_UNSIGNED_SHORT,
                            palet.getIndices());

                    // disable the enabled arrays
                    GLES20.glDisableVertexAttribArray(vertexHandle);
                    GLES20.glDisableVertexAttribArray(normalHandle);
                    GLES20.glDisableVertexAttribArray(textureCoordHandle);

                    SampleUtils.checkGLError("Render Frame");
                }
            //}
        }
    }

    private void printUserData(Trackable trackable) {
        String userData = (String) trackable.getUserData();
        Log.d(LOGTAG, "UserData:Retrieved User Data	\"" + userData + "\"");
    }

    public void setTextures(Vector<Texture> textures) {
        this.textures = textures;
    }
}
