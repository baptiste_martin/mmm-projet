package mmm.m2gla.istic.fr.palet.model;

import java.util.ArrayList;

/**
 * Created by david on 19/03/15.
 */
public interface PartieModel {

    public int  getNbTeams();
    public int  getTeamPoint(int teamNum);
    public void  setPointToWinTeam();
    public int  getFirstTeam();
    public int getCurTeam();
    public int paletLaunched();
    public ArrayList<TeamModel> getTeams();
}
